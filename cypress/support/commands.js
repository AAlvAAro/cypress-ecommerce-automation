// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add("visitPath", (pathName) => {
  cy.visit(`${Cypress.env("host")}${pathName}`);
});

Cypress.Commands.add("login", (email, password) => {
  cy.visitPath("/login");
  cy.get("#spree_user_email").type(email, { sensitive: true });
  cy.get("#spree_user_password").type(password, { sensitive: true });
  cy.get(".button").click();
});

Cypress.Commands.add("loginUser", () => {
  cy.login(Cypress.env("userEmail"), Cypress.env("userPassword"));
});

Cypress.Commands.add("logoutUser", () => {
  cy.visitPath("/");
  cy.get('a[href="/logout"]').click();
});

Cypress.Commands.overwrite("type", (originalFn, element, text, options) => {
  if (options && options.sensitive) {
    // turn off original log
    options.log = false;

    // create your own log with masked message
    Cypress.log({
      $el: element,
      name: "type",
      message: "*".repeat(text.length),
    });
  }

  return originalFn(element, text, options);
});
